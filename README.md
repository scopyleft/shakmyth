## Run locally

The project is based on Docker Compose for local development.

You should run `docker compose up`.

## Database

Create the database on your container:

```
docker compose exec db mysql -u root -pdummy_password -e "create database shakmyth;"
```

Get a copy of the Mysql database and call the file shakmyth.sql (for the example below).

```
docker compose exec -iT db mysql -u root -pdummy_password shakmyth < shakmyth.sql
```

## Env variable

Create .env by duplicating .env.example file:

```
cp .env.example .env
```
