FROM php:5.5.35-fpm-alpine

# We add busybox-suid to allow cmd wih su postgres -c
RUN apk update && apk --no-cache --update add nginx bash postgresql-dev postgresql busybox-suid

# PHP_CPPFLAGS are used by the docker-php-ext-* scripts
ENV PHP_CPPFLAGS="$PHP_CPPFLAGS -std=c++11"

RUN docker-php-ext-install pdo pdo_mysql && docker-php-ext-enable pdo.so pdo_mysql.so
RUN { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=2'; \
        echo 'opcache.fast_shutdown=1'; \
        echo 'opcache.enable_cli=1'; \
    } > /usr/local/etc/php/conf.d/php-opocache-cfg.ini

COPY nginx.conf /etc/nginx/nginx.conf
COPY entrypoint.sh /etc/entrypoint.sh
COPY . /app
RUN chown -R www-data:www-data /app
WORKDIR /app

ENTRYPOINT ["sh", "/etc/entrypoint.sh"]
